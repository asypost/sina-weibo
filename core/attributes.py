#encoding=utf-8
import functools

class AsyncAttribute(object):
    '''
    Just a attribute for determine if the function need to be called aysnc
    '''
    def __init__(self,func):
        super(AsyncAttribute,self).__init__()
        assert callable(func)
        functools.update_wrapper(self,func)
        self.__func=func

    def __call__(self,*args,**kwargs):
        return self.__func(*args,**kwargs)