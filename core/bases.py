#encoding=utf-8

import urlparse,urllib
import uuid

class Singleton(type):
    def __init__(cls,name,bases,dic):
        super(Singleton,cls).__init__(name,bases,dic)
        cls.instance=None

    def __call__(cls,*args,**kwargs):
        if cls.instance is None:
            cls.instance=super(Singleton,cls).__call__(*args,**kwargs)
        return cls.instance

class JSLikeObject(object):
    def __init__(self):
        super(JSLikeObject,self).__init__()

    def __getitem__(self,key):
        if hasattr(self,key):
            item=getattr(self,key)
            return item
        raise AttributeError("%s has no attribute %s" %(repr(self),key))
        
    def __getattribute__(self,key):
        value=super(JSLikeObject,self).__getattribute__(key)
        return value

    def __setitem__(self,key,value):
        if hasattr(self,key):
            setattr(self,key,value)
        else:
            self.__dict__[key]=value

    def get(self,key,fallback=None):
        if hasattr(self,key):
            return self[key]
        else:
            return fallback

    def has_key(self,key):
        return hasattr(self,key)

class Request(object):
    request_id_field="__request_id__"

    def __init__(self,path,**kwargs):
        self.path=path
        self.__mem=MemStorage()
        self.params=kwargs
        param="&".join([urllib.quote_plus(i)+"="+urllib.quote_plus(str(kwargs[i])) for i in kwargs])
        self.__uri="".join((path,"?",param)) if param else path
        self.__id=str(uuid.uuid4())

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self,value):
        self.__id=value

    @property
    def uri(self):
        return ("&" if self.__uri.find("?")!=-1 else "?").join((self.__uri,"=".join((self.request_id_field,self.id))))

    def get_response(self):
        if not self.__mem.has_key(self.id):
            self.__mem[self.id]=Response(self.id)
        return self.__mem[self.id]


    @staticmethod
    def from_uri(uri):
        params=uri.split("?",1)
        path=params[0]
        param=dict(urlparse.parse_qsl(params[1])) if len(params)==2 else dict()
        request_id=param.get(Request.request_id_field,"");
        if(param.has_key(Request.request_id_field)):del param[Request.request_id_field]
        request=Request(path,**param)
        request.id=request_id
        return request

class Response(JSLikeObject):
    def __init__(self,request_id):
        super(Response,self).__init__()
        self.request_id=request_id

class URIHandler(object):
    proptocol=None

    def __init__(self,webview=None):
        super(URIHandler,self).__init__()
        self.webview=webview

    def handle_uri(self,uri,*args,**kwargs):
        raise NotImplementedError()

class MemStorage(dict):
    __metaclass__=Singleton