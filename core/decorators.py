#encoding=utf-8

import logging
import functools
from threading import Thread

def debug(func):
    logger=logging.getLogger("Debugger")
    @functools.wraps(func)
    def _(*args,**kwargs):
        message="Call func<{0}>:args={1},kwargs={2}".\
            format(func.__name__,str(args),str(kwargs))
        logger.debug(message)
        return func(*args,**kwargs)
    return _

try:
    from gi.repository import GLib,Gdk
    #init glib and gdk threads support
    GLib.threads_init()
    Gdk.threads_init()
    idle_add=GLib.idle_add
except:
    try:
        import gtk
        #init gtk threads support
        gtk.threads_init()
        idle_add=gtk.idle_add
    except:
        idle_add=None

if idle_add:
    def async(callback):
        @functools.wraps(callback)
        def _(func):
            @functools.wraps(func)
            def __(*args,**kwargs):
                worker=Thread()
                worker.setDaemon(True)
                def run():
                    return idle_add(callback,*func(*args,**kwargs))
                worker.run=run
                worker.start()
            return __
        return _
else:
    def async(callback):
        @functools.wraps(callback)
        def _(func):
            @functools.wraps(func)
            def __(*args,**kwargs):
                return callback(func(*args,**kwargs))
            return __
        return _