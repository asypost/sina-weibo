#encoding=utf-8

try:
    from webkit import WebView
except:
    from webkit.webkit import WebView
import logging
from bases import Request
import webbrowser

logger=logging.getLogger("WeiboView")

class WebUIView(WebView):
    """
    A custom WebView for ui
    """
    __gtype_name__="GWebUIView"

    def __init__(self):
        super(WebUIView,self).__init__()
        self.__handlers=dict()
        self.__setup()
        self.__connect_signals()

    def __validate_uri_handler(self,handler):
        handler_func_attr="handle_uri"
        protocol_attr="protocol"
        assert (hasattr(handler,handler_func_attr) \
            and callable(getattr(handler,handler_func_attr))\
            and hasattr(handler,protocol_attr))==True


    def register_uri_handler(self,handler):
        self.__validate_uri_handler(handler)
        if not self.__handlers.has_key(handler.protocol):
            self.__handlers[handler.protocol]=handler
            logger.info("URI handler registered:{0}".format(handler.protocol))
        else:
            raise KeyError("Handler already exists:{0}".format(handler.protocol))

    def update_uri_handler(self,handler):
        self.__validate_uri_handler(handler)
        self.__handlers[handler.protocol]=handler

    def __setup(self):
        enables=("enable-universal-access-from-file-uris",
            "javascript-can-access-clipboard",
            "enable-page-cache",
           "tab-key-cycles-through-elements",
           "enable-file-access-from-file-uris")

        disables=("enable-spell-checking",
            "enable-default-context-menu",
            "enable-caret-browsing")

        settings=self.get_settings()
        for prop in enables:
            try:
                settings.set_property(prop,True)
            except:
                logger.warn("Set property error:{0}".format(prop))
        for prop in disables:
            try:
                settings.set_property(prop,False)
            except:
                logger.warn("Set property error:{0}".format(prop))


    def __connect_signals(self):
        self.connect("navigation-requested",self.on_navigation_requested)
        self.connect("new-window-policy-decision-requested",self.on_new_window_requested)
        #self.connect("script-alert",self.on_script_alert)
        self.connect("load-finished",self.on_load_finished)
        self.connect("title-changed",self.on_title_changed)

    def __uri_protocol(self,uri):
        return (uri.split("://",1)[0]+"://").lower()

    def open_in_system_browser(self,uri):
        return webbrowser.open(uri)

    def on_title_changed(self,view,frame,title):
        handler=self.get_handler_for(title)
        if handler:
            handler.handle_uri(title)
            return True
        return False

    def on_navigation_requested(self,view,webframe,request):
        handler=self.get_handler_for(request.get_uri())
        if handler:
            handler.handle_uri(request.get_uri())
            return True
        else:
            if self.__uri_protocol(request.get_uri())!="file://":
                return self.open_in_system_browser(request.get_uri())
        return False

    def on_new_window_requested(self,view,frame,request,decision,u_data):
        return self.open_in_system_browser(request.get_uri())

    def on_script_alert(self,view,webframe,message):
        self.do_request(Request("ui://alert",message=message))
        return True

    def get_handler_for(self,uri):
        return self.__handlers.get(self.__uri_protocol(uri),None)

    def do_request(self,request):
        handler=self.get_handler_for(request.uri)
        if handler:
            self.load_uri(request.uri)
            return True
        return False

    def on_load_finished(self,view,webframe):
        pass
