#encoding=utf-8

from gi.repository import Gdk,Gtk,GdkPixbuf,GLib
from threading import Thread
from gettext import gettext as _
import os
import urllib2
import logging
from gi.repository import WebKit
from core.bases import Request
from util.humanize import pretty_data_size

logger=logging.getLogger("Widgets")

class ImageViewer(Gtk.Window):
    """docstring for ImageViewer"""
    def __init__(self, url,parent=None):
        super(ImageViewer, self).__init__()
        self.pixbuf_loader=None
        self.url=url
        self.origin_animation=None
        self.running=True
        self.downloaded_size=0
        if parent:
            self.set_transient_for(parent)
        self.main_box=Gtk.Box.new(Gtk.Orientation.VERTICAL,0)
        self.set_default_size(600,400)
        self.set_title(_("Image"))
        self.set_position(Gtk.WindowPosition.CENTER)
        self.image_view=Gtk.Image.new()
        view_port=Gtk.Viewport()
        self.scrolled_window=Gtk.ScrolledWindow()
        view_port.add(self.image_view)
        self.scrolled_window.add(view_port)

        #statusbar
        self.status_bar=Gtk.Statusbar()
        self.__download_conext_desc="downloading"
        self.image_scale=Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL,1,300,1)
        self.status_bar.pack_end(self.image_scale,False,False,0)
        self.image_scale.set_size_request(165,20)
        self.image_scale.set_value_pos(Gtk.PositionType.RIGHT)
        self.image_scale.set_visible(False)
        #signals
        self.image_scale.connect("value-changed",self.on_image_scale_value_changed)
        self.image_scale.connect("format-value",self.on_image_scale_format_value)


        #toolbar
        self.toolbar=Gtk.Toolbar()
        self.tool_save_button=Gtk.ToolButton.new_from_stock(Gtk.STOCK_SAVE)
        self.toolbar.insert(self.tool_save_button,0)
        self.tool_zoom_100_button=Gtk.ToolButton.new_from_stock(Gtk.STOCK_ZOOM_100)
        self.toolbar.insert(self.tool_zoom_100_button,0)
        self.tool_zoom_100_button.set_sensitive(False)
        self.tool_zoom_fit_button=Gtk.ToolButton.new_from_stock(Gtk.STOCK_ZOOM_FIT)
        self.toolbar.insert(self.tool_zoom_fit_button,0)
        self.tool_zoom_fit_button.set_sensitive(False)
        self.tool_copy_url_button=Gtk.ToolButton.new_from_stock(Gtk.STOCK_COPY)
        self.toolbar.insert(self.tool_copy_url_button,0)
        #signals
        self.tool_save_button.connect("clicked",self.on_save_button_clicked)
        self.tool_copy_url_button.connect("clicked",self.on_copy_url_button_clicked)
        self.tool_zoom_100_button.connect("clicked",self.on_zoom_100_button_clicked)
        self.tool_zoom_fit_button.connect("clicked",self.on_zoom_fit_button_clicked)

        self.main_box.pack_start(self.toolbar,False,True,0)
        self.main_box.pack_start(self.scrolled_window,True,True,0)
        self.main_box.pack_end(self.status_bar,False,True,0)
        self.add(self.main_box)
        self.start_download(self.url)
        self.connect("destroy",self.on_destroy)

    def on_image_scale_value_changed(self,widget,data=None):
        if self.origin_animation:
            scale_rate=self.image_scale.get_value()/100.0
            static_image=self.origin_animation.get_static_image()
            static_image=static_image.scale_simple(static_image.get_width()*scale_rate,\
                                                   static_image.get_height()*scale_rate,
                                                   GdkPixbuf.InterpType.BILINEAR)
            self.image_view.set_from_pixbuf(static_image)


    def on_image_scale_format_value(self,widget,value,data=None):
        return "{0:0.2f}%".format(value)


    def on_copy_url_button_clicked(self,widget,data=None):
        clipboard=Gtk.Clipboard.get_for_display(Gdk.Display.get_default(),Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(self.url,-1)
        clipboard.store()

    def on_zoom_100_button_clicked(self,widget,data=None):
        self.image_scale.set_value(100)

    def on_zoom_fit_button_clicked(self,widget,data=None):
        if self.origin_animation:
            static_image=self.origin_animation.get_static_image()
            image_rate=float(static_image.get_width())/static_image.get_height()
            window_rate=float(self.scrolled_window.get_allocated_width())/self.scrolled_window.get_allocated_height()
            if image_rate>window_rate:
                scale_rate=float(static_image.get_width())/self.scrolled_window.get_allocated_width()
            else:
                scale_rate=float(static_image.get_height())/self.scrolled_window.get_allocated_height()
            self.image_scale.set_value(100.0/scale_rate)

    def on_save_button_clicked(self,widget,data=None):
        if self.url:
            dialog=Gtk.FileChooserDialog(_("Save Image"),
                                             self,
                                             Gtk.FileChooserAction.SAVE,
                                             (Gtk.STOCK_CANCEL,Gtk.ResponseType.CANCEL,
                                             Gtk.STOCK_SAVE,Gtk.ResponseType.ACCEPT))
            dialog.set_do_overwrite_confirmation(True)
            if dialog.run()==Gtk.ResponseType.ACCEPT:
                filename=dialog.get_filename()
                ext=os.path.split(self.url)[1]
                filename+=ext;
                try:
                    def run():
                        data=urllib2.urlopen(self.url).read()
                        f=open(filename,"wb")
                        f.write(data)
                        f.close()
                    downloader=Thread()
                    downloader.run=run
                    downloader.start()
                except Exception,e:
                    message_dialog=Gtk.MessageDialog(self.main_window,Gtk.DialogFlags.DESTROY_WITH_PARENT,
                          Gtk.MessageType.ERROR,Gtk.ButtonsType.OK,_("Save Image Failed"))
                    message_dialog.set_title(_("Sina micorblog"))
                    message_dialog.run()
                    message_dialog.destroy()
                    logger.log(e)
            dialog.destroy()


    def on_destroy(self,data=None):
        self.running=False
        if self.pixbuf_loader:
            try:
                self.pixbuf_loader.close()
            except Exception,e:
                logger.debug(e)

    def update_widgets_status(self):
        """
        Update widgets states;
        for zoom buttons:they are enabled only
        if the GtkImage contains a satic image
        """
        button_zoom_sensitive=True if self.origin_animation and \
                self.origin_animation.is_static_image() else False
        self.tool_zoom_100_button.set_sensitive(button_zoom_sensitive)
        self.tool_zoom_fit_button.set_sensitive(button_zoom_sensitive)
        self.image_scale.set_visible(button_zoom_sensitive)
        if button_zoom_sensitive:
            self.image_scale.set_value(100)


    def update_image(self,loader,data=None):
            try:
                ani=loader.get_animation()
                self.image_view.set_from_animation(ani)
            except Exception,e:
                logger.debug(e)

    def on_image_downloading(self,data):
        self.downloaded_size+=len(data)
        context_id=self.status_bar.get_context_id(self.__download_conext_desc)
        self.status_bar.remove_all(context_id)
        self.status_bar.push(context_id,_("Downloading:{0}").format\
                (pretty_data_size(self.downloaded_size)))
        self.pixbuf_loader.write(data)

    def on_image_downloaded(self):
        context_id=self.status_bar.get_context_id(self.__download_conext_desc)
        self.status_bar.remove_all(context_id)
        self.status_bar.push(context_id,_("Image File Size:{0}").format\
                (pretty_data_size(self.downloaded_size)))
        self.update_image(self.pixbuf_loader)
        self.pixbuf_loader.close()
        self.origin_animation=self.image_view.get_animation()
        self.pixbuf_loader=None
        self.ajust_size(self.origin_animation)
        self.update_widgets_status()

    def on_image_download_failed(self,message):
        dialog=Gtk.MessageDialog(self,Gtk.DialogFlags.DESTROY_WITH_PARENT,
                          Gtk.MessageType.ERROR,None,message)
        dialog.set_title(_("Sina micorblog"))
        dialog.set_modal(True)
        close_button=dialog.add_button(Gtk.STOCK_CLOSE,Gtk.ResponseType.CLOSE)
        def close(button,data=None):
            dialog.destroy()
            self.destroy()
        close_button.connect("clicked",close)
        dialog.show_all()

    def ajust_size(self,image):
        if not image:return False

        pixbuf_width=image.get_width()
        pixbuf_height=image.get_height()
        screen_width=self.get_screen().get_width()
        screen_height=self.get_screen().get_height()
        width,height=self.get_size()
        toolbar_height=self.toolbar.get_allocated_height()

        if pixbuf_width<=screen_width:
            width=pixbuf_width
        if  pixbuf_height<=screen_height:
            height=pixbuf_height

        height+=toolbar_height+self.get_border_width()+self.status_bar.get_allocated_height()
        width+=self.get_border_width()
        self.resize(width,height)

    def start_download(self,url):
        def run():
            try:
                buffer_size=1024
                stream=urllib2.urlopen(url)
                data=stream.read(buffer_size)
                while data and self.running:
                    GLib.idle_add(self.on_image_downloading,data)
                    data=stream.read(buffer_size)
                GLib.idle_add(self.on_image_downloaded)
            except Exception,e:
                logger.debug(e)
                GLib.idle_add(self.on_image_download_failed,_("Loading Image Failed"))
        if not self.pixbuf_loader:
            self.pixbuf_loader=GdkPixbuf.PixbufLoader()
            self.pixbuf_loader.connect("area-prepared",self.update_image)
        thread=Thread()
        thread.setDaemon(True)
        thread.run=run
        thread.start()


class AuthorizeDialog(Gtk.Dialog):
    def __init__(self,parent=None):
        super(AuthorizeDialog,self).__init__()
        if parent:
            self.set_transient_for(parent)
        self.set_destroy_with_parent(True)
        self.set_title(_("Authorize"))
        self.set_modal(True)
        self.set_default_size(300,400)
        self.__webview=WebKit.WebView()
        self.scrolled_window=Gtk.ScrolledWindow.new(None,None)
        self.__webview.connect("navigation-requested",self.on_navigation_request)
        self.get_content_area().add(self.__webview)
        self.access_token_uri=None
        self.code=None
        self.show_all()

    def open(self,author_uri,access_token_uri):
        self.access_token_uri=access_token_uri
        self.__webview.open(author_uri)
        return self.run()

    def on_navigation_request(self,view,frame,request):
        url=request.get_uri()
        if url.startswith(self.access_token_uri):
            r=Request.from_uri(url)
            code=r.params["code"]
            self.code=code
            self.response(Gtk.ResponseType.ACCEPT)
            return True
        return False
