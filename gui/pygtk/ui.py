#!/usr/bin/env python2
#encoding=utf-8

import gtk
import logging
from core.bases import URIHandler
from core.bases import Request
from core.view_pywebkit import WebUIView
from core.configuration import config
from gettext import gettext as _
from core.decorators import debug
import os
import json
from notify import Notification
from core.attributes import AsyncAttribute
from core.decorators import async
from widgets import ImageViewer,AuthorizeDialog

gtk.gdk.threads_init()
gtk.threads_init()

FORMAT = '[%(levelname)s] %(name)s: %(message)s'

logging.basicConfig(format=FORMAT,level=logging.DEBUG)
logger=logging.getLogger("UIHandler")

class WeiboUI(URIHandler):
    protocol="ui://"

    def __init__(self):
        super(WeiboUI,self).__init__()

        self.main_window=gtk.Window()
        self.main_window.set_icon_name("gwibber")
        self.main_window.set_title(_("Sina micorblog"))
        self.main_window.set_default_size(970,600)

        self.weibo_view=WebUIView()
        self.weibo_view.register_uri_handler(self)

        self.scrolled_window=gtk.ScrolledWindow(None,None)

        self.main_window.add(self.scrolled_window)
        self.scrolled_window.add(self.weibo_view)

        template=open(os.path.join(config.resources_dir,"index.html")).read()
        self.weibo_view.load_string(template,"text/html","UTF-8","file://"+os.path.join(config.resources_dir,"index.html"))

        self.main_window.show_all()

        self.__connect_signals()

    def pending_response(self,request,response):
        response_json=json.dumps({"request_id":request.id,"data":response})
        self.weibo_view.execute_script("Native.Contact.pendingResponse({0})".format(response_json))
        return False

    def handle_request_async(self,request,func):
        data=func(self,**request.params)
        return request,data

    def handle_uri(self,uri,*args,**kwargs):
        if self.is_support(uri):
            request=Request.from_uri(uri)
            action_array=request.path[len(self.protocol):].split("/")
            action=action_array[0] if action_array else None
            try:
                if action:
                    action=getattr(self,action)
                    if isinstance(action,AsyncAttribute):
                        async(self.pending_response)(self.handle_request_async)(request,action)
                    else:
                        data= action(**request.params)
                        self.pending_response(request,data)
            except Exception as e:
                logger.error("Call {0}({1}) error:{2}".format(action,request.params,e))

    def is_support(self,uri):
        ret=False
        if uri.lower().startswith(self.protocol):
            tmp=uri[len(self.protocol):].split("?")
            cmd=tmp[0]
            action=cmd.split("/")[0]
            ret=hasattr(self,action) and callable(getattr(self,action))
        ret or logger.warn("Unsupport URI:{0}".format(uri))
        return ret

    def log(self,log_level,log):
        log_levels={"info":logger.info,\
                    "warn":logger.warn,\
                    "error":logger.error}
        _log=log_levels.get(log_level,None)
        if _log:
            _log(log)

    @debug
    def show_image(self,url):
        image_view=ImageViewer(url)
        image_view.show_all()

    @AsyncAttribute
    @debug
    def alert(self,message=None):
        if not message:return True
        gtk.threads_enter()
        dialog=gtk.MessageDialog(self.main_window,gtk.DIALOG_DESTROY_WITH_PARENT,
                          gtk.MESSAGE_INFO,gtk.BUTTONS_OK,message)
        dialog.set_title(_("Sina micorblog"))
        dialog.run()
        dialog.destroy()
        gtk.threads_leave()
        return True

    @debug
    def show_authorize_dialog(self,auhorize_url,code_url):
        logger.debug("start Authorize dialog")
        dialog=AuthorizeDialog(self.main_window)
        code=None
        if dialog.open(auhorize_url,code_url)==gtk.RESPONSE_ACCEPT:
            code=dialog.code if dialog.code else ""
        dialog.destroy()
        return code

    def get_notification(self):
        return Notification(_("Sina Micorblog"))

    def tooltip(self,title=None,message=None):
        pass

    def notify(self,summary,body,icon,image=None):
        notification=self.get_notification()
        notification.update(summary,body,icon)
        if image:
            notification.set_image(image)
        notification.show()

    def __connect_signals(self):
        self.main_window.connect("destroy",self.app_quit)

    def app_quit(self,data=None):
        return gtk.main_quit()
