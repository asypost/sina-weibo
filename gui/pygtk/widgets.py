#encoding=utf-8

import gtk
from gettext import gettext as _
from webkit.webkit import WebView
import logging
import urllib2
from core.bases import Request
from threading import Thread

logger=logging.getLogger("Widgets")

class ImageViewer(gtk.Window):
    """docstring for ImageViewer"""
    def __init__(self, url,parent=None):
        super(ImageViewer, self).__init__()
        self.pixbuf_loader=None
        self.running=True
        if parent:
            self.set_transient_for(parent)
        self.set_default_size(600,400)
        self.set_title(_("Image"))
        self.set_position(gtk.WIN_POS_CENTER_ALWAYS)
        self.image_view=gtk.Image()
        view_port=gtk.Viewport()
        scrolled_window=gtk.ScrolledWindow()
        scrolled_window.add(view_port)
        view_port.add(self.image_view)
        self.add(scrolled_window)
        self.image_view.show()
        self.start_download(url)
        self.connect("destroy",self.on_destroy)

    def on_destroy(self,data=None):
        self.running=False
        if self.pixbuf_loader:
            try:
                self.pixbuf_loader.close()
            except Exception,e:
                logger.debug(e)

    def update_image(self,loader,data=None):
            try:
                ani=loader.get_animation()
                self.image_view.set_from_animation(ani)
            except Exception,e:
                logger.debug(e)

    def on_image_downloading(self,data):
        self.pixbuf_loader.write(data)

    def on_image_downloaded(self):
        self.update_image(self.pixbuf_loader)
        self.pixbuf_loader.close()
        self.pixbuf_loader=None
        self.ajust_size()

    def on_image_download_failed(self,message):
        dialog=gtk.MessageDialog(self,gtk.DIALOG_DESTROY_WITH_PARENT,
                          gtk.MESSAGE_ERROR,None,message)
        dialog.set_title(_("Sina micorblog"))
        dialog.set_modal(True)
        close_button=dialog.add_button(gtk.STOCK_CLOSE,gtk.RESPONSE_CLOSE)
        def close(button,data=None):
            dialog.destroy()
            self.destroy()
        close_button.connect("clicked",close)
        dialog.show_all()

    def ajust_size(self):
        if not self.image_view.get_animation():
            return False
        pixbuf_width=self.image_view.get_animation().get_width()
        pixbuf_height=self.image_view.get_animation().get_height()
        screen_width=self.get_screen().get_width()
        screen_height=self.get_screen().get_height()
        width,height=self.get_size()
        if pixbuf_width<=screen_width:
            width=pixbuf_width
        if  pixbuf_height<=screen_height:
            height=pixbuf_height
        self.resize(width,height)

    def start_download(self,url):
        def run():
            try:
                buffer_size=1024
                stream=urllib2.urlopen(url)
                data=stream.read(buffer_size)
                while data and self.running:
                    gtk.idle_add(self.on_image_downloading,data)
                    data=stream.read(buffer_size)
                gtk.idle_add(self.on_image_downloaded)
            except Exception,e:
                print(e)
                logger.debug(e)
                gtk.idle_add(self.on_image_download_failed,_("Loading Image Failed"))
        if not self.pixbuf_loader:
            self.pixbuf_loader=gtk.gdk.PixbufLoader()
            self.pixbuf_loader.connect("area-prepared",self.update_image)
        thread=Thread()
        thread.setDaemon(True)
        thread.run=run
        thread.start()


class AuthorizeDialog(gtk.Dialog):
    def __init__(self,parent=None):
        super(AuthorizeDialog,self).__init__()
        if parent:
            self.set_transient_for(parent)
        self.set_destroy_with_parent(True)
        self.set_title(_("Authorize"))
        self.set_modal(True)
        self.set_default_size(300,400)
        self.__webview=WebView()
        self.scrolled_window=gtk.ScrolledWindow(None,None)
        self.__webview.connect("navigation-requested",self.on_navigation_request)
        self.get_content_area().add(self.__webview)
        self.access_token_uri=None
        self.code=None
        self.show_all()

    def open(self,author_uri,access_token_uri):
        self.access_token_uri=access_token_uri
        self.__webview.open(author_uri)
        return self.run()

    def on_navigation_request(self,view,frame,request):
        url=request.get_uri()
        if url.startswith(self.access_token_uri):
            r=Request.from_uri(url)
            code=r.params["code"]
            self.code=code
            self.response(gtk.RESPONSE_ACCEPT)
            return True
        return False
