#!/usr/bin/env python2

def pygtk_backend():
    from gui.pygtk import WeiboUI
    import gtk
    gtk.threads_enter()
    WeiboUI()
    gtk.main()
    gtk.threads_leave()

def pygobject_backend():
    import gi
    gi.require_version("Gtk","3.0")
    gi.require_version("WebKit","3.0")
    from gui.gi import WeiboUI
    from gi.repository import Gtk,Gdk
    Gdk.threads_enter()
    Gtk.init(sys.argv)
    WeiboUI()
    Gtk.main()
    Gdk.threads_leave()

def auto_detect():
    try:
        pygobject_backend()
    except:
        pygtk_backend()

def backend(name):
    if name=="pygtk":
        pygtk_backend()
    elif name=="pygobject":
        pygobject_backend()
    else:
        auto_detect()

if __name__=="__main__":
    import sys
    import os
    import gettext
    from argparser import ArgParser

    gettext.bindtextdomain("SinaWeibo",os.path.join(os.path.dirname(__file__),"resources/i18n"))
    gettext.textdomain('SinaWeibo')

    parser=ArgParser(default=auto_detect)
    parser.bind({"backend":backend})
    parser.feed(sys.argv)

