#!/usr/bin/env python
#encoding=utf-8
from abc import *

class NotificationBase(object):
    __metaclass__=ABCMeta
    def __init__(self, app_name):
        super(NotificationBase, self).__init__()
        self.app_name = app_name

    @abstractmethod
    def update(self,*args,**kwargs):
        pass

    @abstractmethod
    def show(self):
        pass

    @abstractmethod
    def set_image(self,image):
        pass
