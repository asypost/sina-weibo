#encoding=utf-8
from base import NotificationBase

class Notification(NotificationBase):
    """Dummy Notification,do nothing"""
    def __init__(self, app_name):
        super(Notification, self).__init__(app_name)

    def update(self,*args,**kwargs):
        pass

    def show(self):
        pass

    def set_image(self,image):
        pass