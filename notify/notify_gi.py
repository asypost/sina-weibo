from gi.repository import Notify
from gi.repository import GdkPixbuf
from base import NotificationBase
import urllib
import logging

logger=logging.getLogger("NotificationGobject")

class Notification(NotificationBase):
    """
    This notification is a linux specified notification.
    Do not use this class directly
    """

    def __init__(self,app_name):
        super(Notification, self).__init__(app_name)
        self.__notification=None

    def init(self):
        if not Notify.init(self.app_name):
                logger.error("Init notification failed")
                return False
        else:
            if not self.__notification:
                self.__notification=Notify.Notification()
                self.__notification.set_app_name(self.app_name)
            return True


    def update(self,*args,**kwargs):
        if self.init():
            self.__notification.update(*args,**kwargs)

    def show(self):
        if self.init():
            self.__notification.show()

    def set_image(self,image):
        if self.init():
            if isinstance(image,basestring):
                try:
                    pixbuf_loader=GdkPixbuf.PixbufLoader()
                    pixbuf_loader.write(urllib.urlopen(image).read())
                    image=pixbuf_loader.get_pixbuf()
                    pixbuf_loader.close()
                except Exception,e:
                    image=None
                    logger.debug(e)
            try:
                if image:
                    self.__notification.set_image_from_pixbuf(image)
            except Exception,e:
                logger.debug(e)
