/**
 * User: anhulife
 * Date: 12-12-09
 * Time: 11:47
 */
module.exports = function (grunt) {
	"use strict";

	// readOptionalJSON
	// by Ben Alman
	// https://gist.github.com/2876125
	function readOptionalJSON(filepath) {
		var data = {};
		try {
			data = grunt.file.readJSON(filepath);
			grunt.verbose.write("Reading " + filepath + "...").ok();
		} catch (e) {
		}
		return data;
	}

	// Project configuration.
	grunt.initConfig({
		meta:{
			banner:"/*! Weibo /license */"
		},
		lint:{
			grunt:"grunt.js",
			src:"js/dist/app.js"
		},
		watch:{
			files:[ 'index.html', 'js/src/**/*.js' ],
			tasks:'concat:dist lint'
		},
		jshint:{
			options:{
				jquery:true,
				browser:true,
				curly:true,
				eqeqeq:true,
				immed:true,
				latedef:true,
				newcap:true,
				noarg:true,
				sub:true,
				undef:true,
				boss:true,
				eqnull:true,
				node:true,
				es5:true
			},
			globals:{
				Backbone:true,
				_:true
			}
		},
		concat:{
			lib:{
				src:[
					'js/lib/jquery-1.8.3.js',
					'js/lib/underscore-1.4.3.js',
					'js/lib/mustache-0.7.1.js',
					'js/lib/backbone-0.9.9.js',
					'js/lib/cacheProvider.js',
					'js/lib/date.format-1.2.3.js'
				],
				dest:'js/dist/libs.js',
				separator:';'
			},
			dist:{
				src:[
					'js/src/app.js',
					'js/src/utils.js',
					'js/src/native.js',
					'js/src/views/base.js',
					'js/src/views/column.js',
					'js/src/views/stream.js',
					'js/src/views/settings.js',
					'js/src/views/home.js',
					'js/src/sina.js',
					'js/src/models/sina.js',
					'js/src/collections/sina.js',
					'js/src/views/sina/tweet.js',
					'js/src/views/sina/stream.js',
					'js/src/views/sina/timeline.js'
				],
				dest:'js/dist/app.js'
			}
		},
		min:{
			lib:{
				src:['<config:concat.lib.dest>'],
				dest:'js/dist/libs.min.js'
			},
			dist:{
				src:['<banner:meta.banner>', '<config:concat.dist.dest>'],
				dest:'js/dist/app.min.js'
			}
		},
		server:{
			port:8080,
			base:'.'
		}
	});

	// Default task.
	grunt.registerTask('default', 'concat:lib min:lib concat:dist server watch');

};