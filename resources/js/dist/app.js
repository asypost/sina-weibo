!function (global, $) {
	var App = {
		Weibo:{},
		Views:{},
		Controllers:{},
		Models:{},
		Collections:{},
		AccountTypes:[],
		Cache:new global.CacheProvider(),
		Widgets:{},
		Pages:{},
		Utils:{},
		Elements:{
			$body:$('body'),
			$mainLoading:$('#main_loading')
		},
		Actions:{
			showLoading:function () {
				App.Elements.$mainLoading.show();
			},
			hideLoading:function () {
				App.Elements.$mainLoading.hide();
			}
		},
		ProcessAjaxs:{},
		initialize:function () {
			this.appCtrl = new App.Controllers.AppCtrl();
			Backbone.history.start();
		}
	};

	//Accounts
	App.Accounts = (function(){
		var accounts = App.Cache.get('accounts', true, true) || {},
			get = function(id){
				return id ? accounts[id] : accounts;
			},
			add = function(account){
				accounts[account._id] = account;
				App.Cache.set('accounts', accounts, true);
				App.Accounts.trigger("add", account);
				return account;
			};
			
		return _.extend({
			get: get,
			add: add
		}, Backbone.Events);
	})();

	// controllers
	App.Controllers.AppCtrl = Backbone.Router.extend({
		routes:{
			'':'home',
			'!/':'home'
		},
		home:function () {
			var home = new App.Views.Home();
			home.render();
		}
	});

	global.App = App;
}(window, window.jQuery);
!function(global, App){
    function getViewportSize() {
        return {
            height:window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            width:window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
        };
    }

    function getViewportOffset() {
        return {
            top:window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
            left:window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft
        };
    }

    function isInView(el) {
        var viewportSize = getViewportSize(),
            viewportOffset = getViewportOffset(),
            $el = $(el),
            offset = $el.offset(),
            width = $el.width(),
            height = $el.height();
        if (offset.left + width > viewportOffset.left &&
            offset.left < viewportOffset.left + viewportSize.width &&
            offset.top + height > viewportOffset.top &&
            offset.top < viewportOffset.top + viewportSize.height) {
            //inview
            return true;
        } else {
            return false;
        }
    }

    //字符串长度限制
    function strShort(name, maxLength) {
        var len = 0,
            arr = name.split(""),
            result = [];
        maxLength = (maxLength || 12) * 2;
        var l = arr.length;
        for (var i = 0; i < l; ++i) {
            if (arr[i].charCodeAt(0) < 299) {
                len++;
            } else {
                len += 2;
            }
            result.push(arr[i]);
            if (len >= maxLength - 2) {
                result.push('..');
                break;
            }
        }
        return result.join('');
    }

    function strLen(str) {
        var len = 0;
        _.each(str.split(''), function (_char, index) {
            if (_char.charCodeAt(0) < 299) {
                len++;
            } else {
                len += 2;
            }
        });
        return len / 2;
    }

    function prettyDate(date){
        date = new Date(date);
        var diff = new Date() - date;

        // 小于十分钟
        if(diff <= 10 * 60 * 1e3){
            return '刚刚';
        }

        // 小于1小时
        if(diff <= 60 * 60 * 1e3){
            return Math.floor(diff / (60 * 1e3)) + '分钟';
        }

        // 小于1天
        if(diff <= 24 * 60 * 60 * 1e3){
            return Math.floor(diff / (60 * 60 * 1e3)) + '小时';
        }

        return Math.floor(diff / (24 * 60 * 60 * 1e3)) + '天';
    }

    App.Utils = _.extend(App.Utils, {
        getViewportSize:getViewportSize,
        getViewportOffset:getViewportOffset,
        isInView:isInView,
        strShort:strShort,
        strLen:strLen,
        prettyDate:prettyDate
    });
}(window, window.App);
!function(global,$){
    var $document=global.document;
    var Native={
        Gui:{},
        Contact:{
            RequestRouter:{}
        }
    };

    Native.Contact.Request=function(url,params){
        /**
        *Request for contact with native objects
        *@param {string} url:url
        *@param {object} params:params of the url
        **/
        this.id=this.guid();
        var request_id_field="__request_id__";
        //add the request id to the query string,so that we can pass it to the python
        //client,and then the response come from the python client,we can call a 
        //callback function related to the request id.
        if(url.indexOf("?")!=-1){
            this.url=url+"&"+request_id_field+"="+this.id;
        }else{
            this.url=url+"?"+request_id_field+"="+this.id;
        }
        if(params){
            for(var p in params){
                if(params.hasOwnProperty(p)){
                    if(params[p]!=undefined && params[p]!=null){
                        this.url=this.url+"&"+p+"="+encodeURIComponent(params[p]);
                    }
                }
            }
        }
    };

    Native.Contact.Request.prototype.guid=function(){
        return _.uniqueId('native_request_');
    };

    Native.Contact.Request.prototype.send=function(callback){
        if($.isFunction(callback)){
            if(Native.Contact.RequestRouter[this.id]){
                throw "The request is already registered"
            }else{
                Native.Contact.RequestRouter[this.id]=callback;
            }
        }
        $document.title=this.url;
    };

    Native.Contact.pendingResponse=function(json){
        var response=json;
        var callback=Native.Contact.RequestRouter[response.request_id];
        if($.isFunction(callback)){
            callback(response);
            delete Native.Contact.RequestRouter[response.request_id];
        }
    };

    Native.Contact.sendRequest=function(url,params,callback){
        new Native.Contact.Request(url,params).send(callback);
    };

    Native.Gui.notify=function(summary,body,icon,image){
        /**
        *Show notification
        *@param {string} summary:the summary of the notification
        *@param {string} body:the body of the notification
        *@param {string} icon:the icon name of the notification,possible<info,error>
        *@param {string} image:the url of the image you'd like to display in the notification
        **/
        var params={summary:summary,body:body,icon:icon,image:image};
        Native.Contact.sendRequest("ui://notify",params);
    };

    Native.Gui.showImage=function(url){
        /**
        *Display the image given by the url
        *@param {string} url:the url of the image you'd like to display
        **/
        if (url) {
            var params={url:url};
            Native.Contact.sendRequest("ui://show_image",params);
        }else{
            throw "Argument url should not be null";
        }
    };

    Native.Gui.alert=function(message){
        /**
        *Show a alert dialog
        *@param {string} message:the message you want to display in the alert dialog
        **/
        if(message){
            var params={message:message};
            Native.Contact.sendRequest("ui://alert",params);
        }else{
            throw "Argument message should not be null";
        }
    };

    Native.Gui.showAuthorizeDialog=function(auhorize_url,code_url,callback){
        /**
        *Show authorize dialog
        *@param {string} auhorize_url:the authorize url
        *@param {string} code_url:the url for getting code
        *@param {function} callback:the callback function name used for getting the code
        **/
        if (auhorize_url && $.isFunction(callback) && code_url) {
            var params={auhorize_url:auhorize_url,code_url:code_url};
            Native.Contact.sendRequest("ui://show_authorize_dialog",params,callback);
        }else{
            throw "Arguments not match";
        }
    };

    Native.Gui.quitApplication=function(){
        /**
        *Quit the application
        **/
        Native.Contact.sendRequest("ui://app_quit");
    };

    global.Native=Native;

}(window,window.jQuery);
!function (global, App) {

	/**
	 * 视图基类
	 */
	App.Views.Base = Backbone.View.extend({
		initialize:function (options) {
			Backbone.View.prototype.initialize.call(this, options);
			options && options.container && (this.container = options.container);
		},
		//模板路径
		partials:{},
		template:'',
		container:null,
		render:function () {
			this.fetch();
			return this;
		},
		//加载数据
		fetch:function () {
			this.data = {};
			this.renderMain(this.data);
		},
		getTemplate:function (path, callback) {
			$.get('templates/' + path, callback);
		},
		fetchTemplatesAndPartials:function (callback) {
			var that = this, size = _.size(this.partials), count = 0, partials = {};
			this.getTemplate(this.template, function (template) {
				if (size) {
					_.each(that.partials, function (url, name) {
						that.getTemplate(url, function (partial) {
							partials[name] = partial;
							++count;
							if (count === size) {
								callback && callback(template, partials);
							}
						});
					});
				} else {
					callback && callback(template, partials);
				}
			});
		},
		//加载数据的回调，主要是渲染数据
		renderMain:function (data) {
			this.fetchTemplatesAndPartials(_.bind(function (template, partials) {
				this._template = template;
				this._partials = partials;
				this.$el.html(Mustache.render(template, data, partials)).appendTo(this.container).show();
				this.afterRender();
			}, this));
		},
		//渲染后的操作，可以用于实例一些DOM引用
		afterRender:function () {
		},
		//删除操作之前调用
		beforeRemove:function () {
		},
		//删除操作
		remove:function () {
			this.beforeRemove();
			this.undelegateEvents();
			this.$el.remove();
			return this;
		}
	});

	App.Views.Modal = App.Views.Base.extend({
		className: 'ovl scroll-v',
		template: 'modal.html',
		title: '',
		container: App.Elements.$body
	});
}(window, window.App);
!function (global, App) {

	/**
	 * 列表基类
	 */
	App.Views.Column = App.Views.Base.extend({
		events: {

		},
		//模板路径
		template:'column.html',
		tagName:"section",
		className:'column',
		fetch:function(){
			this.renderMain({title:this.title});
		},
		afterRender:function () {
			this.$header = this.$('[data-selector="header"]');
			this.$options = this.$('[data-selector="options"]');
			this.$stream = this.$('[data-selector="stream"]');
			this.$scroll = this.$('.scroll-v');
			this.$detail = this.$('[data-selector="detail"]');
			this.$loading = this.$('[data-selector="loading"]');
		}
	});
}(window, window.App);
!function(global, App){
	App.Views.Stream = App.Views.Base.extend({
		className: 'stream-container',
		count: 20,
		hasOldItem: true,
		updatesCallback: null,
		fetchNew : function(){
			if(this.fetchNewLock){
				return;
			}
			this.fetchNewLock = true;
		},
		fetchOld : function () {
			if(this.fetchOldLock){
				return;
			}
			this.$loading.show();
			this.fetchOldLock = true;
		},
		afterRender:function(){
			this.$scroll = this.$el.parents('.scroll-v');
			this.$loading = $('<div class="is-actionable loading" style="display: none;" data-selector="loading">加载中.....</div>').appendTo(this.container);
			this.newItems = [];

			this.fetchOld();

			this.fetchNew && (this.fetchTime = setInterval(_.bind(this.fetchNew, this), 10 * 1e3));
			this.$scroll.bind('scroll', _.bind(this.check, this));
		},
		fetchNewCallback : function(items){
			this.fetchNewLock = false;
			if(items){
				this.newItems.push(items);
				if(this.$scroll.scrollTop() < 50){
					this.addNewItems();
				}else{
					this.options.updatesCallback && this.options.updatesCallback();
				}
			}
		},
		fetchOldCallback: function(data){
			this.$loading.hide();
			this.fetchOldLock = false;
			if(!this.hasOldItem){
				this.$scroll.unbind('scroll');
			}

			this.$el.append($(Mustache.render(this._template, data, this._partials)));
		},
		addNewItems:function(){
			var that = this;
			_.each(this.newItems, function(item){
				$(Mustache.render(that._template, item, that._partials)).prependTo(that.$el);
			});
			this.newItems = [];
			this.options.cancelUpdatesCallback && this.options.cancelUpdatesCallback();
		},
		check:function(evt){
			if(this.$scroll.scrollTop() < 50 && this.newItems.length){
				this.addNewItems();
			}

			if(this.hasOldItem && this.$el.height() - this.$scroll.scrollTop() - this.$scroll.height() < 50){
				this.fetchOld();
			}
		},
		beforeRemove:function(){
			this.$scroll.unbind('scroll');
			clearInterval(this.fetchTime);
		}
	});
}(window, window.App);
!
function(global, App) {
	App.Views.Settings = App.Views.Modal.extend({
		events: {
			'click [data-action="close"]': 'remove',
			'click [name="add_account"]': 'addAccount'
		},
		partials: {
			content: 'settings.html'
		},
		accountTemplate: '<li> <div class="js-account list-account cf"> <img src="{{profile_image_url}}" alt="{{name}}" class="avatar"> <div class="content"><strong class="fullname">{{screen_name}}</strong><span class="username"><span class="at">@</span>{{name}}</span></div></div></li>',
		title: 'Application Settings',
		fetch: function() {
			this.renderMain({
				title: this.title,
				accounts: _.values(App.Accounts.get()),
				accountTypes: App.AccountTypes
			});
		},
		afterRender: function() {
			this.$service = this.$('select[name="service"]');
			this.$accounts = this.$('ul[data-selector="account-container"]');
		},
		addAccount: function() {
			var service = this.$service.val(),
				accountType = _.find(App.AccountTypes, function(accountType) {
					return accountType.type === service;
				});

			Native.Gui.showAuthorizeDialog(accountType.url.authorize, accountType.url.code, _.bind(function(res) {
				accountType.getAccessToken(res.data, _.bind(function(res) {
					var accessToken = res.access_token,
						uid = res.uid;
					accountType.getUserById(uid, accessToken, _.bind(function(user) {
						var account = user.toJSON();
						account.accessToken = accessToken;
						account.type = service;
						account._id = service + uid;
						$(Mustache.to_html(this.accountTemplate, account)).appendTo(this.$accounts);
						App.Accounts.add(account);
					}, this));
				}, this));
			}, this));
		}
	});
}(window, window.App);
!function (global, App) {
	App.Views.Home = App.Views.Base.extend({
		events:{
			'click [data-action="settings-menu"]' : 'showSettingsMenu'
		},
		template:'home.html',
		container:App.Elements.$body,
		afterRender:function () {
			var accounts = App.Accounts.get();
			if(accounts && _.size(accounts)){
				var columns = App.Cache.get('columns', true, true);
				if (columns && columns.length) {

				} else {
					var account = _.find(accounts, function(){return true;}),
						homeTimeline = new App.Views[account.type].HomeTimeline({
							account:account,
							container:this.$('#columns')
						});
					homeTimeline.render();
				}
			}else{
				this.showSettingsMenu();
				App.Accounts.once("add", _.bind(function(account){
					var homeTimeline = new App.Views[account.type].HomeTimeline({
							account:account,
							container:this.$('#columns')
						});
					homeTimeline.render();
				}, this));
			}
		},
		showSettingsMenu: function(){
			var settings = new App.Views.Settings();
			settings.render();
		}
	});
}(window, window.App);
!
function(global, App) {
	App.Views.Sina = {};
	App.Widgets.Sina = {};

	App.Weibo.Sina = {
		type: 'Sina',
		label: 'Sina',
		appKey: '2220221549',
		appSecret: 'd75ef035dda834cf7a60680388ebbf54',
		url: {
			api: 'https://api.weibo.com/2',
			authorize: (function() {
				return 'https://api.weibo.com/oauth2/authorize?' + $.param({
					display: 'mobile',
					client_id: '2220221549',
					redirect_uri: 'https://api.weibo.com/oauth2/default.html'
				});
			})(),
			token: 'https://api.weibo.com/oauth2/access_token',
			code: 'https://api.weibo.com/oauth2/default.html',
			user: {
				show: 'https://api.weibo.com/2/users/show.json'
			},
			reply: {
				show: 'https://api.weibo.com/2/comments/show.json'
			},
			status: {
				homeTimeline: 'https://api.weibo.com/2/statuses/home_timeline.json',
				show : 'https://api.weibo.com/2/statuses/show.json'
			}
		},
		getUserById: function(uid, accessToken, callback){
			var user = new App.Models.Sina.User();
			user.fetch({
				data: {
					access_token: accessToken,
					uid: uid
				},
				success: callback
			});
		},
		getAccessToken: function(code, callback) {
			$.post(App.Weibo.Sina.url.token, {
				client_id: App.Weibo.Sina.appKey,
				client_secret: App.Weibo.Sina.appSecret,
				grant_type: 'authorization_code',
				code: code,
				redirect_uri: App.Weibo.Sina.url.code
			}, function(res) {
				callback && callback(res);
			}, 'json');
		}
	};

	App.AccountTypes.push(App.Weibo.Sina);
}(window, window.App);
!function (global, App) {
	App.Models.Sina = {
		Tweet:Backbone.Model.extend({
			url: App.Weibo.Sina.url.status.show,
			defaults:{
				prettyCreatedTime:function () {
					return App.Utils.prettyDate(this.created_at);
				},
				createdTime:function () {
					return new Date(this.created_at).format('yyyy-mm-dd HH:MM:ss');
				}
			}
		}),
		Reply:Backbone.Model.extend({
			url: App.Weibo.Sina.url.reply.show,
			defaults:{
				prettyCreatedTime:function () {
					return App.Utils.prettyDate(this.created_at);
				},
				createdTime:function () {
					return new Date(this.created_at).format('yyyy-mm-dd HH:MM:ss');
				}
			}
		}),
		User: Backbone.Model.extend({
			url: App.Weibo.Sina.url.user.show
		})
	};
}(window, window.App);
!function (global, App) {
	App.Collections.Sina = {
		TweetStream : Backbone.Collection.extend({
			initialize : function (models, options) {
				Backbone.Collection.prototype.initialize.call(this, models, options);
				this.url = options.url;
			},
			model : App.Models.Sina.Tweet,
			parse : function (res) {
				this.totalNumber = res.total_number;
				return res.statuses;
			}
		}),
		Replies : Backbone.Collection.extend({
			url:App.Weibo.Sina.url.reply.show,
			model : App.Models.Sina.Reply,
			parse : function (res) {
				this.totalNumber = res.total_number;
				return res.comments;
			}
		})
	};
}(window, window.App);
!function (global, App) {
	App.Widgets.Sina.TweetDetail = App.Views.Base.extend({
		events : {
			'click header[data-action="back"]' : 'back',
			'scroll .js-detail-container' : 'check',
			'resize .js-detail-container' : 'check'
		},
		template : 'sina/tweetDetail.html',
		partials : {tweet : 'sina/tweet.html'},
		fetch : function () {
			this.tweet = new App.Models.Sina.Tweet();
			this.tweet.fetch({
				data:{
					access_token : this.options.account.accessToken,
					id: this.options.tweetId
				},
				success: _.bind(function(){
					var retweet;
					if(this.tweet.get('retweeted_status')){
						retweet = new App.Models.Sina.Tweet(this.tweet.get('retweeted_status')).toJSON();
					}
					this.renderMain({title : this.options.title, tweet : this.tweet.toJSON(), retweet : retweet});
				}, this)
			});
		},
		afterRender:function(){
			this.$replies = this.$('[data-selector="replies"]');
			this.replies = new App.Widgets.Sina.ReplyStream({
				account: this.options.account,
				tweetId: this.options.tweetId,
				container: this.$replies
			});
			this.replies.render();
		},
		back : function () {
			this.options.backCallback && this.options.backCallback();
			this.remove();
		},
		check: function(evt){
			console.log(evt);
		}
	});
}(window, window.App);
!function (global, App) {
	App.Widgets.Sina.TweetStream = App.Views.Stream.extend({
		template : 'sina/tweetStream.html',
		partials : {tweet : 'sina/tweet.html'},
		fetchNew : function () {
			App.Views.Stream.prototype.fetchNew.call(this);
			var tweets = new App.Collections.Sina.TweetStream(null, {
				url : this.options.url
			});

			tweets.fetch({
				data : {
					access_token : this.options.account.accessToken,
					count : this.count,
					since_id : this.tweets.length && this.tweets.first().get('id')
				},
				success : _.bind(function () {
					var length = this.tweets.length, diff;
					this.tweets.add(tweets.models, {at : 0});

					diff = this.tweets.length - length;
					tweets.models = this.tweets.filter(function (tweet, index) {
						return index < diff;
					});

					this.fetchNewCallback(tweets.length ? {tweets : tweets.toJSON()} : null);
				}, this)
			});
		},
		fetchOld : function () {
			App.Views.Stream.prototype.fetchOld.call(this);
			this.tweets = this.tweets || new App.Collections.Sina.TweetStream(null, {
				url : this.options.url
			});

			var tweets = new App.Collections.Sina.TweetStream(null, {
				url : this.options.url
			});

			tweets.fetch({
				data : {
					access_token : this.options.account.accessToken,
					count : this.count,
					max_id : this.tweets.length && this.tweets.last().get('id')
				},
				success : _.bind(function () {
					var length = this.tweets.length;
					this.tweets.add(tweets.models);

					if (tweets.length === this.count && this.tweets.length < tweets.totalNumber) {
						this.hasOldItem = true;
					} else {
						this.hasOldItem = false;
					}

					tweets.models = this.tweets.filter(function (tweet, index) {
						return index >= length;
					});

					this.fetchOldCallback({tweets : tweets.toJSON()});
				}, this)
			});
		}
	});

	App.Widgets.Sina.ReplyStream = App.Views.Stream.extend({
		template : 'sina/replyStream.html',
		partials : {reply : 'sina/reply.html'},
		fetchNew : null,
		fetchOld : function () {
			App.Views.Stream.prototype.fetchOld.call(this);
			this.replies = this.replies || new App.Collections.Sina.Replies();
			var replies = new App.Collections.Sina.Replies();
			replies.fetch({
				data : {
					access_token : this.options.account.accessToken,
					id : this.options.tweetId,
					count : this.count,
					max_id : this.replies.length && this.replies.last().get('id')
				},
				success : _.bind(function () {
					var length = this.replies.length;
					this.replies.add(replies.models);


					if (replies.length === this.count && this.replies.length < replies.totalNumber) {
						this.hasOldItem = true;
					} else {
						this.hasOldItem = false;
					}

					replies.models = this.replies.filter(function (reply, index) {
						return index >= length;
					});

					this.fetchOldCallback({replies : replies.toJSON()});
				}, this)
			});
		}
	});
}(window, window.App);
!function (global, App) {
	App.Views.Sina.HomeTimeline = App.Views.Column.extend({
		events : {
			'click article[data-tweet-id]' : 'statusDetail',
			'click [data-action="resetToTopColumn"]' : 'resetToTopColumn',
			'click a[rel="user"]' : 'userDetail'
		},
		title : 'Home',
		afterRender : function () {
			App.Views.Column.prototype.afterRender.call(this);

			var that = this;

			this.stream = new App.Widgets.Sina.TweetStream({
				container : this.$stream,
				account : this.options.account,
				url : App.Weibo.Sina.url.status.homeTimeline,
				updatesCallback : function(){
					that.$('.column-updates').addClass('is-visible');
				},
				cancelUpdatesCallback:function(){
					that.$('.column-updates').removeClass('is-visible');
				}
			});

			this.stream.render();
		},
		statusDetail : function (evt) {
			var $el = $(evt.currentTarget),
				tweetId = $el.data('tweetId');
			this.tweetDetail = new App.Widgets.Sina.TweetDetail({
				container : this.$detail,
				title : this.title,
				tweetId : tweetId,
				account : this.options.account,
				backCallback : _.bind(function () {
					this.$el.removeClass('is-shifted-1');
					this.tweetDetail.remove();
					this.tweetDetail = null;
				}, this)
			});
			this.tweetDetail.render();
			this.$el.addClass('is-shifted-1');
		},
		userDetail : function (evt) {
			var $el = $(evt.currentTarget), tweet;
			evt.preventDefault();
			evt.stopPropagation();
		},
		resetToTopColumn:function(){
			this.$scroll.animate({
				scrollTop: 0
			});
		},
		beforeRemove : function () {
			this.stream.remove();
		}
	});
}(window, window.App);