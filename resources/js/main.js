!function(globel, $){
    var update_dialog=new Dialog("#status_dialog","Update",true),
        current_panel=null;

    $("#action_timeline").click(function(){
        showTab("#timeline");
        updateTimeline();
        current_panel="timeline";
    });
    $("#action_mentions").click(function()
    {
        showTab("#mentions");
        updateMentionsMe();
        current_panel="mentions";
    });
    $("#notification_panel").click(function()
    {
        closeNotification();
    });
    $("#load_more").click(function(){
        if(current_panel=="timeline"){
            olderTimeline();
        }else if(current_panel=="mentions"){
            olderMentions();
        }
    });
    $("#action_update").click(function(){
        update_dialog.show();
        return false;
    });

    new weibo.util.Request("weibo://check_authoriztion").send(function(response)
    {
        if(response.data.result){
            showTab("#timeline");
            current_panel="timeline";
            updateTimeline();
            autoUpdate();
        }else{
            var params={"auhorize_url":response.data.authorize_url,"code_url":response.data.code_url};
            new weibo.util.Request("ui://show_authorize_dialog",params).send();
        }
    });


    globel.update = function(){
        var text=$("#status_text").val();
        update_dialog.close();
        new weibo.util.Request("weibo://update",{"status":text}).send(function(response){
            if(!response.data.result){
                alert("send failed");
            }else{
                var html=response.data.html;
                if($("#timeline").children().length==0){
                    $("#timeline").append(html);
                }else{
                    $(html).insertBefore($("#timeline").children()[0]);
                }
                $("#status_text").val("");
            }
        });
    };
}(window, window.jQuery);