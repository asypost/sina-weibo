!function (global, App) {
	App.Collections.Sina = {
		TweetStream : Backbone.Collection.extend({
			initialize : function (models, options) {
				Backbone.Collection.prototype.initialize.call(this, models, options);
				this.url = options.url;
			},
			model : App.Models.Sina.Tweet,
			parse : function (res) {
				this.totalNumber = res.total_number;
				return res.statuses;
			}
		}),
		Replies : Backbone.Collection.extend({
			url:App.Weibo.Sina.url.reply.show,
			model : App.Models.Sina.Reply,
			parse : function (res) {
				this.totalNumber = res.total_number;
				return res.comments;
			}
		})
	};
}(window, window.App);