!function(global,$){
    var $document=global.document;
    var Native={
        Gui:{},
        Contact:{
            RequestRouter:{}
        }
    };

    Native.Contact.Request=function(url,params){
        /**
        *Request for contact with native objects
        *@param {string} url:url
        *@param {object} params:params of the url
        **/
        this.id=this.guid();
        var request_id_field="__request_id__";
        //add the request id to the query string,so that we can pass it to the python
        //client,and then the response come from the python client,we can call a 
        //callback function related to the request id.
        if(url.indexOf("?")!=-1){
            this.url=url+"&"+request_id_field+"="+this.id;
        }else{
            this.url=url+"?"+request_id_field+"="+this.id;
        }
        if(params){
            for(var p in params){
                if(params.hasOwnProperty(p)){
                    if(params[p]!=undefined && params[p]!=null){
                        this.url=this.url+"&"+p+"="+encodeURIComponent(params[p]);
                    }
                }
            }
        }
    };

    Native.Contact.Request.prototype.guid=function(){
        return _.uniqueId('native_request_');
    };

    Native.Contact.Request.prototype.send=function(callback){
        if($.isFunction(callback)){
            if(Native.Contact.RequestRouter[this.id]){
                throw "The request is already registered"
            }else{
                Native.Contact.RequestRouter[this.id]=callback;
            }
        }
        $document.title=this.url;
    };

    Native.Contact.pendingResponse=function(json){
        var response=json;
        var callback=Native.Contact.RequestRouter[response.request_id];
        if($.isFunction(callback)){
            callback(response);
            delete Native.Contact.RequestRouter[response.request_id];
        }
    };

    Native.Contact.sendRequest=function(url,params,callback){
        new Native.Contact.Request(url,params).send(callback);
    };

    Native.Gui.notify=function(summary,body,icon,image){
        /**
        *Show notification
        *@param {string} summary:the summary of the notification
        *@param {string} body:the body of the notification
        *@param {string} icon:the icon name of the notification,possible<info,error>
        *@param {string} image:the url of the image you'd like to display in the notification
        **/
        var params={summary:summary,body:body,icon:icon,image:image};
        Native.Contact.sendRequest("ui://notify",params);
    };

    Native.Gui.showImage=function(url){
        /**
        *Display the image given by the url
        *@param {string} url:the url of the image you'd like to display
        **/
        if (url) {
            var params={url:url};
            Native.Contact.sendRequest("ui://show_image",params);
        }else{
            throw "Argument url should not be null";
        }
    };

    Native.Gui.alert=function(message){
        /**
        *Show a alert dialog
        *@param {string} message:the message you want to display in the alert dialog
        **/
        if(message){
            var params={message:message};
            Native.Contact.sendRequest("ui://alert",params);
        }else{
            throw "Argument message should not be null";
        }
    };

    Native.Gui.showAuthorizeDialog=function(auhorize_url,code_url,callback){
        /**
        *Show authorize dialog
        *@param {string} auhorize_url:the authorize url
        *@param {string} code_url:the url for getting code
        *@param {function} callback:the callback function name used for getting the code
        **/
        if (auhorize_url && $.isFunction(callback) && code_url) {
            var params={auhorize_url:auhorize_url,code_url:code_url};
            Native.Contact.sendRequest("ui://show_authorize_dialog",params,callback);
        }else{
            throw "Arguments not match";
        }
    };

    Native.Gui.quitApplication=function(){
        /**
        *Quit the application
        **/
        Native.Contact.sendRequest("ui://app_quit");
    };

    global.Native=Native;

}(window,window.jQuery);