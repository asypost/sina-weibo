!function(global, App){
    function getViewportSize() {
        return {
            height:window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            width:window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
        };
    }

    function getViewportOffset() {
        return {
            top:window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
            left:window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft
        };
    }

    function isInView(el) {
        var viewportSize = getViewportSize(),
            viewportOffset = getViewportOffset(),
            $el = $(el),
            offset = $el.offset(),
            width = $el.width(),
            height = $el.height();
        if (offset.left + width > viewportOffset.left &&
            offset.left < viewportOffset.left + viewportSize.width &&
            offset.top + height > viewportOffset.top &&
            offset.top < viewportOffset.top + viewportSize.height) {
            //inview
            return true;
        } else {
            return false;
        }
    }

    //字符串长度限制
    function strShort(name, maxLength) {
        var len = 0,
            arr = name.split(""),
            result = [];
        maxLength = (maxLength || 12) * 2;
        var l = arr.length;
        for (var i = 0; i < l; ++i) {
            if (arr[i].charCodeAt(0) < 299) {
                len++;
            } else {
                len += 2;
            }
            result.push(arr[i]);
            if (len >= maxLength - 2) {
                result.push('..');
                break;
            }
        }
        return result.join('');
    }

    function strLen(str) {
        var len = 0;
        _.each(str.split(''), function (_char, index) {
            if (_char.charCodeAt(0) < 299) {
                len++;
            } else {
                len += 2;
            }
        });
        return len / 2;
    }

    function prettyDate(date){
        date = new Date(date);
        var diff = new Date() - date;

        // 小于十分钟
        if(diff <= 10 * 60 * 1e3){
            return '刚刚';
        }

        // 小于1小时
        if(diff <= 60 * 60 * 1e3){
            return Math.floor(diff / (60 * 1e3)) + '分钟';
        }

        // 小于1天
        if(diff <= 24 * 60 * 60 * 1e3){
            return Math.floor(diff / (60 * 60 * 1e3)) + '小时';
        }

        return Math.floor(diff / (24 * 60 * 60 * 1e3)) + '天';
    }

    App.Utils = _.extend(App.Utils, {
        getViewportSize:getViewportSize,
        getViewportOffset:getViewportOffset,
        isInView:isInView,
        strShort:strShort,
        strLen:strLen,
        prettyDate:prettyDate
    });
}(window, window.App);