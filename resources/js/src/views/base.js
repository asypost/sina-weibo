!function (global, App) {

	/**
	 * 视图基类
	 */
	App.Views.Base = Backbone.View.extend({
		initialize:function (options) {
			Backbone.View.prototype.initialize.call(this, options);
			options && options.container && (this.container = options.container);
		},
		//模板路径
		partials:{},
		template:'',
		container:null,
		render:function () {
			this.fetch();
			return this;
		},
		//加载数据
		fetch:function () {
			this.data = {};
			this.renderMain(this.data);
		},
		getTemplate:function (path, callback) {
			$.get('templates/' + path, callback);
		},
		fetchTemplatesAndPartials:function (callback) {
			var that = this, size = _.size(this.partials), count = 0, partials = {};
			this.getTemplate(this.template, function (template) {
				if (size) {
					_.each(that.partials, function (url, name) {
						that.getTemplate(url, function (partial) {
							partials[name] = partial;
							++count;
							if (count === size) {
								callback && callback(template, partials);
							}
						});
					});
				} else {
					callback && callback(template, partials);
				}
			});
		},
		//加载数据的回调，主要是渲染数据
		renderMain:function (data) {
			this.fetchTemplatesAndPartials(_.bind(function (template, partials) {
				this._template = template;
				this._partials = partials;
				this.$el.html(Mustache.render(template, data, partials)).appendTo(this.container).show();
				this.afterRender();
			}, this));
		},
		//渲染后的操作，可以用于实例一些DOM引用
		afterRender:function () {
		},
		//删除操作之前调用
		beforeRemove:function () {
		},
		//删除操作
		remove:function () {
			this.beforeRemove();
			this.undelegateEvents();
			this.$el.remove();
			return this;
		}
	});

	App.Views.Modal = App.Views.Base.extend({
		className: 'ovl scroll-v',
		template: 'modal.html',
		title: '',
		container: App.Elements.$body
	});
}(window, window.App);