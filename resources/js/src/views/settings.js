!
function(global, App) {
	App.Views.Settings = App.Views.Modal.extend({
		events: {
			'click [data-action="close"]': 'remove',
			'click [name="add_account"]': 'addAccount'
		},
		partials: {
			content: 'settings.html'
		},
		accountTemplate: '<li> <div class="js-account list-account cf"> <img src="{{profile_image_url}}" alt="{{name}}" class="avatar"> <div class="content"><strong class="fullname">{{screen_name}}</strong><span class="username"><span class="at">@</span>{{name}}</span></div></div></li>',
		title: 'Application Settings',
		fetch: function() {
			this.renderMain({
				title: this.title,
				accounts: _.values(App.Accounts.get()),
				accountTypes: App.AccountTypes
			});
		},
		afterRender: function() {
			this.$service = this.$('select[name="service"]');
			this.$accounts = this.$('ul[data-selector="account-container"]');
		},
		addAccount: function() {
			var service = this.$service.val(),
				accountType = _.find(App.AccountTypes, function(accountType) {
					return accountType.type === service;
				});

			Native.Gui.showAuthorizeDialog(accountType.url.authorize, accountType.url.code, _.bind(function(res) {
				accountType.getAccessToken(res.data, _.bind(function(res) {
					var accessToken = res.access_token,
						uid = res.uid;
					accountType.getUserById(uid, accessToken, _.bind(function(user) {
						var account = user.toJSON();
						account.accessToken = accessToken;
						account.type = service;
						account._id = service + uid;
						$(Mustache.to_html(this.accountTemplate, account)).appendTo(this.$accounts);
						App.Accounts.add(account);
					}, this));
				}, this));
			}, this));
		}
	});
}(window, window.App);