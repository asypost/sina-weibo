!function (global, App) {
	App.Widgets.Sina.TweetStream = App.Views.Stream.extend({
		template : 'sina/tweetStream.html',
		partials : {tweet : 'sina/tweet.html'},
		fetchNew : function () {
			App.Views.Stream.prototype.fetchNew.call(this);
			var tweets = new App.Collections.Sina.TweetStream(null, {
				url : this.options.url
			});

			tweets.fetch({
				data : {
					access_token : this.options.account.accessToken,
					count : this.count,
					since_id : this.tweets.length && this.tweets.first().get('id')
				},
				success : _.bind(function () {
					var length = this.tweets.length, diff;
					this.tweets.add(tweets.models, {at : 0});

					diff = this.tweets.length - length;
					tweets.models = this.tweets.filter(function (tweet, index) {
						return index < diff;
					});

					this.fetchNewCallback(tweets.length ? {tweets : tweets.toJSON()} : null);
				}, this)
			});
		},
		fetchOld : function () {
			App.Views.Stream.prototype.fetchOld.call(this);
			this.tweets = this.tweets || new App.Collections.Sina.TweetStream(null, {
				url : this.options.url
			});

			var tweets = new App.Collections.Sina.TweetStream(null, {
				url : this.options.url
			});

			tweets.fetch({
				data : {
					access_token : this.options.account.accessToken,
					count : this.count,
					max_id : this.tweets.length && this.tweets.last().get('id')
				},
				success : _.bind(function () {
					var length = this.tweets.length;
					this.tweets.add(tweets.models);

					if (tweets.length === this.count && this.tweets.length < tweets.totalNumber) {
						this.hasOldItem = true;
					} else {
						this.hasOldItem = false;
					}

					tweets.models = this.tweets.filter(function (tweet, index) {
						return index >= length;
					});

					this.fetchOldCallback({tweets : tweets.toJSON()});
				}, this)
			});
		}
	});

	App.Widgets.Sina.ReplyStream = App.Views.Stream.extend({
		template : 'sina/replyStream.html',
		partials : {reply : 'sina/reply.html'},
		fetchNew : null,
		fetchOld : function () {
			App.Views.Stream.prototype.fetchOld.call(this);
			this.replies = this.replies || new App.Collections.Sina.Replies();
			var replies = new App.Collections.Sina.Replies();
			replies.fetch({
				data : {
					access_token : this.options.account.accessToken,
					id : this.options.tweetId,
					count : this.count,
					max_id : this.replies.length && this.replies.last().get('id')
				},
				success : _.bind(function () {
					var length = this.replies.length;
					this.replies.add(replies.models);


					if (replies.length === this.count && this.replies.length < replies.totalNumber) {
						this.hasOldItem = true;
					} else {
						this.hasOldItem = false;
					}

					replies.models = this.replies.filter(function (reply, index) {
						return index >= length;
					});

					this.fetchOldCallback({replies : replies.toJSON()});
				}, this)
			});
		}
	});
}(window, window.App);