!function (global, App) {
	App.Views.Sina.HomeTimeline = App.Views.Column.extend({
		events : {
			'click article[data-tweet-id]' : 'statusDetail',
			'click [data-action="resetToTopColumn"]' : 'resetToTopColumn',
			'click a[rel="user"]' : 'userDetail'
		},
		title : 'Home',
		afterRender : function () {
			App.Views.Column.prototype.afterRender.call(this);

			var that = this;

			this.stream = new App.Widgets.Sina.TweetStream({
				container : this.$stream,
				account : this.options.account,
				url : App.Weibo.Sina.url.status.homeTimeline,
				updatesCallback : function(){
					that.$('.column-updates').addClass('is-visible');
				},
				cancelUpdatesCallback:function(){
					that.$('.column-updates').removeClass('is-visible');
				}
			});

			this.stream.render();
		},
		statusDetail : function (evt) {
			var $el = $(evt.currentTarget),
				tweetId = $el.data('tweetId');
			this.tweetDetail = new App.Widgets.Sina.TweetDetail({
				container : this.$detail,
				title : this.title,
				tweetId : tweetId,
				account : this.options.account,
				backCallback : _.bind(function () {
					this.$el.removeClass('is-shifted-1');
					this.tweetDetail.remove();
					this.tweetDetail = null;
				}, this)
			});
			this.tweetDetail.render();
			this.$el.addClass('is-shifted-1');
		},
		userDetail : function (evt) {
			var $el = $(evt.currentTarget), tweet;
			evt.preventDefault();
			evt.stopPropagation();
		},
		resetToTopColumn:function(){
			this.$scroll.animate({
				scrollTop: 0
			});
		},
		beforeRemove : function () {
			this.stream.remove();
		}
	});
}(window, window.App);