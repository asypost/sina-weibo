!function (global, App) {
	App.Widgets.Sina.TweetDetail = App.Views.Base.extend({
		events : {
			'click header[data-action="back"]' : 'back',
			'scroll .js-detail-container' : 'check',
			'resize .js-detail-container' : 'check'
		},
		template : 'sina/tweetDetail.html',
		partials : {tweet : 'sina/tweet.html'},
		fetch : function () {
			this.tweet = new App.Models.Sina.Tweet();
			this.tweet.fetch({
				data:{
					access_token : this.options.account.accessToken,
					id: this.options.tweetId
				},
				success: _.bind(function(){
					var retweet;
					if(this.tweet.get('retweeted_status')){
						retweet = new App.Models.Sina.Tweet(this.tweet.get('retweeted_status')).toJSON();
					}
					this.renderMain({title : this.options.title, tweet : this.tweet.toJSON(), retweet : retweet});
				}, this)
			});
		},
		afterRender:function(){
			this.$replies = this.$('[data-selector="replies"]');
			this.replies = new App.Widgets.Sina.ReplyStream({
				account: this.options.account,
				tweetId: this.options.tweetId,
				container: this.$replies
			});
			this.replies.render();
		},
		back : function () {
			this.options.backCallback && this.options.backCallback();
			this.remove();
		},
		check: function(evt){
			console.log(evt);
		}
	});
}(window, window.App);