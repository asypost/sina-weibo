!function(global, App){
	App.Views.Stream = App.Views.Base.extend({
		className: 'stream-container',
		count: 20,
		hasOldItem: true,
		updatesCallback: null,
		fetchNew : function(){
			if(this.fetchNewLock){
				return;
			}
			this.fetchNewLock = true;
		},
		fetchOld : function () {
			if(this.fetchOldLock){
				return;
			}
			this.$loading.show();
			this.fetchOldLock = true;
		},
		afterRender:function(){
			this.$scroll = this.$el.parents('.scroll-v');
			this.$loading = $('<div class="is-actionable loading" style="display: none;" data-selector="loading">加载中.....</div>').appendTo(this.container);
			this.newItems = [];

			this.fetchOld();

			this.fetchNew && (this.fetchTime = setInterval(_.bind(this.fetchNew, this), 10 * 1e3));
			this.$scroll.bind('scroll', _.bind(this.check, this));
		},
		fetchNewCallback : function(items){
			this.fetchNewLock = false;
			if(items){
				this.newItems.push(items);
				if(this.$scroll.scrollTop() < 50){
					this.addNewItems();
				}else{
					this.options.updatesCallback && this.options.updatesCallback();
				}
			}
		},
		fetchOldCallback: function(data){
			this.$loading.hide();
			this.fetchOldLock = false;
			if(!this.hasOldItem){
				this.$scroll.unbind('scroll');
			}

			this.$el.append($(Mustache.render(this._template, data, this._partials)));
		},
		addNewItems:function(){
			var that = this;
			_.each(this.newItems, function(item){
				$(Mustache.render(that._template, item, that._partials)).prependTo(that.$el);
			});
			this.newItems = [];
			this.options.cancelUpdatesCallback && this.options.cancelUpdatesCallback();
		},
		check:function(evt){
			if(this.$scroll.scrollTop() < 50 && this.newItems.length){
				this.addNewItems();
			}

			if(this.hasOldItem && this.$el.height() - this.$scroll.scrollTop() - this.$scroll.height() < 50){
				this.fetchOld();
			}
		},
		beforeRemove:function(){
			this.$scroll.unbind('scroll');
			clearInterval(this.fetchTime);
		}
	});
}(window, window.App);