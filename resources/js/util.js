window.weibo={};weibo.util={};//namespace weibo.util

weibo.util.routerMap={};//router for request router

//Request class that communicates with python client
weibo.util.Request=function(url,params){
    this.id=this.guid();
    //add the request id to the query string,so that we can pass it to the python
    //client,and then the response come from the python client,we can call a 
    //callback function related to the request id.
    if(url.indexOf("?")!=-1){
        this.url=url+"&"+"__request_id__="+this.id;
    }else{
        this.url=url+"?__request_id__="+this.id;
    }
    if(params){
        for(var p in params){
            if(params.hasOwnProperty(p)){
                this.url=this.url+"&"+p+"="+encodeURIComponent(params[p]);
            }
        }
    }
};

weibo.util.Request.prototype.guid=function(){
    return Math.uuid();
};

weibo.util.Request.prototype.send=function(callback){
    if(callback){
        if(weibo.util.routerMap[this.id]){
            alert("oops,unexcepting error occus when binding callback to a request");
        }else{
            weibo.util.routerMap[this.id]=callback;
        }
    }
    document.title=this.url;
};


weibo.util.defaultResponseHandler=function(response){
    //pass
};

weibo.util.pendingResponse=function(json){
    var response=json;
    var callback=weibo.util.routerMap[response.request_id];
    if(callback){
        callback(response);
        delete weibo.util.routerMap[response.request_id];
    }else{
        weibo.util.defaultResponseHandler(response);
    }
};
