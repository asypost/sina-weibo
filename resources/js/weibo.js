function Dialog(id,title,closeable){
    this.id=id;
    title=title?title:"";
    this.x=0;
    this.y=0;
    this.isDrag=false;
    this.closeable=closeable;
    var body=$("<div class='dialog'></div>");
    this.header=$("<div class='dialog-header'></div>");
    this.title=$("<div class='title'>"+title+"</div>");
    var content=$("<div class='dialog-content'></div>");

    if(closeable){
        this.closeButton=$("<button class='close-button'></button>");
        this.closeButton.css("float","right");
        this.header.append(this.closeButton);
    }

    body.css("display","none");
    body.css("position","fixed");
    this.header.append(this.title);
    body.append(this.header);
    content.append($(id));
    body.append(content);
    this.body=body;
};

Dialog.prototype.setTitle=function(title){
    this.title.text(title);
};

Dialog.prototype.resize=function(){
        this.body.css("left",($(window).width()-this.body.width())/2);
        this.body.css("top",($(window).height()-$("#toolbar").height()-this.body.height())/2);
    };

Dialog.prototype.show=function(){
    if(this.closeable){
        this.closeButton.click(function()
        {
            this.body.remove();
        }.bind(this));
    };
    $(window).resize(this.resize);
    $(document.body).append(this.body);
    this.body.css("display","block");
    this.resize();
    this.header.mousedown(function(event){
        this.isDrag=true;
        this.x=event.offsetX;
        this.y=event.offsetY;
    }.bind(this));

    $(document.body).mousemove(function(event){
        if(this.isDrag){
            this.body.css("left",event.pageX-this.x);
            this.body.css("top",event.pageY-this.y);
        }
    }.bind(this));

    this.header.mouseup(function(){
        this.isDrag=false;
    }.bind(this));
};

Dialog.prototype.close=function(){
    this.body.remove();
};

function showImage(url){
    if(url){
        new weibo.util.Request("ui://show_image",{"url":url}).send();
    };
};

function showTab(id){
    $("#tabs").children().css("display","none");
    $(id).css("display","block");
};

function updateTimeline(){
    notify("Refreshing...");
    new weibo.util.Request("weibo://timeline").send(function(response){
        if($("#timeline").children().length==0){
            $("#timeline").append(response.data.html);
        }else{
            for(var i=0;i<$("#timeline").children().length;i++){
                var item=$("#timeline").children()[i];
                var id=parseInt(item.id.replace("status_",""));
                if(id<=response.data.since_id){
                    $(response.data.html).insertBefore(item);
                    break;
                }
            }
        }
            //$(response.data.html).insertBefore($("#timeline").children()[0]);
        closeNotification();
    });
};

function olderTimeline(){
    notify("Refreshing...");
    new weibo.util.Request("weibo://timeline?type=old").send(function(response){
        if($("#timeline").children().length==0){
            $("#timeline").append(response.data.html);
        }else{
            for(var i=0;i<$("#timeline").children().length;i++){
                var item=$("#timeline").children()[i];
                var id=parseInt(item.id.replace("status_",""));
                if(id==response.data.max_id){
                    $(response.data.html).insertAfter(item);
                    break;
                }
            }
        }
            //$(response.data.html).insertBefore($("#timeline").children()[0]);
        closeNotification();
    });
};

function olderMentions(){
    notify("Refreshing...");
    new weibo.util.Request("weibo://mentions?type=old").send(function(response){
        if($("#mentions").children().length==0){
            $("#mentions").append(response.data.html);
        }else{
            for(var i=0;i<$("#mentions").children().length;i++){
                var item=$("#mentions").children()[i];
                var id=parseInt(item.id.replace("status_",""));
                if(id==response.data.max_id){
                    $(response.data.html).insertAfter(item);
                    break;
                }
            }
        }
        closeNotification();
    });
};

function autoUpdate(){
    setTimeout(function(){
        notify("Refreshing...");
        updateTimeline();
        updateMentionsMe();
        autoUpdate();
        closeNotification();
    },90000);
};

function updateMentionsMe(func){
    notify("Refreshing...");
    new weibo.util.Request("weibo://mentions").send(function(response){
        if(response.data.result){
            if($("#mentions").children().length==0){
                $("#mentions").append(response.data.html);
            }else{
                for(var i=0;i<$("#mentions").children().length;i++){
                var item=$("#mentions").children()[i];
                var id=parseInt(item.id.replace("status_",""));
                if(id<=response.data.since_id){
                    $(response.data.html).insertBefore(item);
                    break;
                }
            }
            }
    }
        closeNotification();
    });
}

function show_comments(id,model){
    var comments_panel=$("#"+model+"_comments_form_"+id);
    var comments_container=$("#"+model+"_comments_for_"+id);
    if(comments_panel.css("display") && comments_panel.css("display")!="none"){
        comments_container.empty();
        comments_panel.css("display","none");
    }else{
        new weibo.util.Request("weibo://comments",{"status_id":id,"model":model,"count":20}).send(function(response){
            if(response.data.result){
                comments_panel.css("display","block");
                comments_container.append(response.data.html);
            }
        });
    }
};

function create_comment(id,model){
    var comment=$("#"+model+"_comment_text_"+id).val();
    var comments_container=$("#"+model+"_comments_for_"+id);
    if(id && comment && model){
        new weibo.util.Request("weibo://create_comment",{"status_id":id,"comment":comment,"model":model}).
        send(function(response){
            if(response.data.result){
                if(comments_container.children().length>0){
                    $(response.data.html).insertBefore(comments_container.children()[0]);
                }else{
                    comments_container.append(response.data.html);
                }
            }
        });
    }
};

function notify(message){
    $("#notification").text(message);
    $("#notification_panel").css("display","block");
}

function closeNotification(){
    $("#notification_panel").css("display","none");
}
