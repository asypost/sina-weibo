#encoding=utf-8
def pretty_data_size(size):
    _format="{0:d}B"
    if size>=2**30:
        size=size/float(2**30)
        _format="{0:0.2f}GB"
    elif size>=2**20:
        size=size/float(2**20)
        _format="{0:0.2f}MB"
    elif size>=2**10:
        size=float(size)/2**10
        _format="{0:0.2f}KB"
    return _format.format(size)
